import 'package:flutter_web/material.dart';
class MyColors {
  static const Color white1 = Color(0xFFF8FBFF);
  static const Color white2 = Color(0xFFFCFDFD);

  static const Color primaryGreen = Color.fromARGB(255, 93, 205, 17);
  static const Color primaryGrey = Color.fromARGB(255, 62, 62, 62);
}