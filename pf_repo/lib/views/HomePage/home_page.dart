import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pf_repo/utils/colors.dart';

import 'package:pf_repo/views/HomePage/components/landing_header.dart';
import 'package:pf_repo/views/HomePage/components/split_view.dart';

import 'components/body.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LandingHeader().build(context),
      body: SplitWidget(
        childFirst: Body(),
        childSecond: RightBody(),
      ),
    );
  }
}
