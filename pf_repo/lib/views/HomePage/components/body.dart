import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pf_repo/utils/colors.dart';
import 'package:pf_repo/views/MainPage/main_body.dart';
import 'package:pf_repo/views/Login/login_screen.dart';
import 'package:pf_repo/views/Signup/signup_screen.dart';

import 'background.dart';
import 'login_round.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // This size provide us total height and width of our screen
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: size.height * 0.05),
            SizedBox(height: size.height * 0.05),
            SizedBox(height: size.height * 0.05),
            RichText(
              text: TextSpan(
                  text: "The Sports Betting",
                  style: TextStyle(
                    fontSize: 36,
                    color: Colors.white,
                    fontStyle: GoogleFonts.shadowsIntoLight().fontStyle,
                  ),
                  children: []),
            ),
            RichText(
              text: TextSpan(
                  text: "Social Media App",
                  style: TextStyle(
                    fontSize: 36,
                    color: Colors.white,
                    fontStyle: GoogleFonts.shadowsIntoLight().fontStyle,
                  ),
                  children: []),
            ),
            SizedBox(height: size.height * 0.05),
            SizedBox(height: size.height * 0.05),
            SizedBox(height: size.height * 0.05),
            SizedBox(height: size.height * 0.05),
            RichText(
              text: TextSpan(
                  text: "Flex your sports knowledge",
                  style: TextStyle(
                    fontSize: 24,
                    color: Colors.white,
                    fontStyle: GoogleFonts.shadowsIntoLight().fontStyle,
                  ),
                  children: []),
            ),
            RichText(
              text: TextSpan(
                  text: "and compete to become #1",
                  style: TextStyle(
                    fontSize: 24,
                    color: Colors.white,
                    fontStyle: GoogleFonts.shadowsIntoLight().fontStyle,
                  ),
                  children: []),
            ),
          ],
        ),
      ),
    );
  }
}

class RightBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // This size provide us total height and width of our screen
    return BackgroundRight(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: size.height * 0.05),
            SizedBox(height: size.height * 0.05),
            SizedBox(height: size.height * 0.05),
            Text(
              "Start FLEXing",
              style: GoogleFonts.yatraOne(
                fontWeight: FontWeight.bold,
                fontSize: 64,
                color: MyColors.primaryGreen,
              ),
            ),
            SizedBox(height: size.height * 0.05),
            SizedBox(height: size.height * 0.05),
            RichText(
              text: TextSpan(
                  text: "Join Sportsflex Today!",
                  style: TextStyle(
                    fontSize: 36,
                    color: Colors.white,
                    fontStyle: GoogleFonts.shadowsIntoLight().fontStyle,
                  ),
                  children: []),
            ),
            SizedBox(height: size.height * 0.05),
            SizedBox(height: size.height * 0.05),
            RoundedLoginButton(
              text: "LOGIN",
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return LoginScreen();
                    },
                  ),
                );
              },
            ),
            RoundedLoginButton(
              text: "SIGN UP",
              color: MyColors.primaryLightGreen,
              textColor: Colors.black,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return SignUpScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
