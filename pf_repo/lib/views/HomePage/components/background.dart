import 'package:flutter/material.dart';

class Background extends StatelessWidget {
  final Widget child;
  const Background({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: double.infinity,
      child: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          child,
        ],
      ),
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage("../lib/assets/images/sports-betting-legal.jpg"),
        fit: BoxFit.cover,
      )),
    );
  }
}

class BackgroundRight extends StatelessWidget {
  final Widget child;
  const BackgroundRight({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: double.infinity,
      child: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          child,
        ],
      ),
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage("../lib/assets/images/lp_background.jpg"),
        fit: BoxFit.cover,
      )),
    );
  }
}
