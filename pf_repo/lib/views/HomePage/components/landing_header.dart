import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pf_repo/utils/colors.dart';
import 'package:pf_repo/utils/strings.dart';

import 'rounded_header.dart';

class LandingHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return AppBar(
        title: Text(
          MyStrings.appTitle,
          style:
              GoogleFonts.yatraOne(color: MyColors.primaryGreen, fontSize: 32),
        ),
        backgroundColor: MyColors.primaryGrey,
        leading: Image(
            image: AssetImage(
          "../lib/assets/images/logo.png",
        )),
        actions: [
          Padding(
            padding: EdgeInsets.all(2.0),
            child: RoundedHeaderInputField(
              hintText: "Your Email",
              onChanged: (value) {},
            ),
          ),
          Padding(
            padding: EdgeInsets.all(2.0),
            child: RoundedHeaderPasswordField(
              onChanged: (value) {},
            ),
          ),
          Padding(
            padding: EdgeInsets.all(2.0),
            child: IconButton(
              icon: Icon(Icons.arrow_right_alt_sharp,
                  color: MyColors.primaryGreen),
              color: MyColors.primaryGreen,
              iconSize: 32,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(2.0),
            child: RoundedHeaderButton(),
          ),
        ]);
  }
}

class LandingHeaderNoField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return AppBar(
      title: Text(
        MyStrings.appTitle,
        style: GoogleFonts.yatraOne(color: MyColors.primaryGreen, fontSize: 32),
      ),
      backgroundColor: MyColors.primaryGrey,
    );
  }
}
