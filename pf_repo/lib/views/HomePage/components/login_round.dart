import 'package:flutter/material.dart';
import 'package:pf_repo/utils/colors.dart';

class RoundedLoginButton extends StatelessWidget {
  final String text;
  final Function press;
  final Color color, textColor;
  const RoundedLoginButton(
      {Key key,
      this.text,
      this.press,
      this.color = MyColors.primaryLightGreen,
      this.textColor = MyColors.primaryGrey})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      width: size.width * 0.4,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(29),
        child: FlatButton(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 40),
          color: color,
          onPressed: press,
          child: Text(
            text,
            style: TextStyle(color: textColor),
          ),
        ),
      ),
    );
  }
}
