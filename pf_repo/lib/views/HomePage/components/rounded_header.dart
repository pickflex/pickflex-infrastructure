import 'package:flutter/material.dart';
import 'package:pf_repo/utils/colors.dart';
import 'package:pf_repo/views/components/text_field_container.dart';

class RoundedHeaderInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChanged;
  const RoundedHeaderInputField({
    Key key,
    this.hintText,
    this.icon = Icons.person,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: TextFieldHeaderContainer(
        child: TextField(
          onChanged: onChanged,
          cursorColor: MyColors.primaryGreen,
          decoration: InputDecoration(
            icon: Icon(
              icon,
              color: MyColors.primaryGreen,
            ),
            hintText: hintText,
            border: InputBorder.none,
          ),
        ),
      ),
    );
  }
}

class RoundedHeaderPasswordField extends StatelessWidget {
  final ValueChanged<String> onChanged;
  const RoundedHeaderPasswordField({
    Key key,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldHeaderContainer(
      child: TextField(
        obscureText: true,
        onChanged: onChanged,
        cursorColor: MyColors.primaryGreen,
        decoration: InputDecoration(
          hintText: "Password",
          icon: Icon(
            Icons.lock,
            color: MyColors.primaryGreen,
          ),
          suffixIcon: Icon(
            Icons.visibility,
            color: MyColors.primaryGreen,
          ),
          border: InputBorder.none,
        ),
      ),
    );
  }
}

class RoundedHeaderButton extends StatelessWidget {
  final ValueChanged<String> onPressed;
  const RoundedHeaderButton({
    Key key,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldHeaderButtonContainer(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(29),
        child: FlatButton(
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
          color: MyColors.primaryGreen,
          onPressed: null,
          child: Text(
            "Sign Up",
            style: TextStyle(color: MyColors.primaryGrey),
          ),
        ),
      ),
    );
  }
}
