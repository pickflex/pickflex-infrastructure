import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pf_repo/utils/colors.dart';
import 'package:pf_repo/views/Feed/feed.dart';
import 'package:pf_repo/views/Leaderboard/leaderboard.dart';
import 'package:pf_repo/views/Login/login_screen.dart';
import 'package:pf_repo/schema/Flex.dart';

import 'package:pf_repo/views/Feed/components/flex_feed.dart';
import 'package:pf_repo/views/NewFlex/newflex.dart';
import 'package:pf_repo/views/Notifications/notifications.dart';
import 'package:pf_repo/views/Profile/profile.dart';
import 'background.dart';

import 'package:http/http.dart';

import 'main_header.dart';

class OuterBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // This size provide us total height and width of our screen
    return Background(child: MainBody());
  }
}

class MainBody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new FlexListState();
  }
}

class FlexListState extends State<MainBody> {
  PageController _pageController;

  int _page = 0;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: MainHeader().build(context),
        backgroundColor: Colors.white,
        bottomNavigationBar: new BottomNavigationBar(
          iconSize: 24,
          type: BottomNavigationBarType.fixed,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.notifications),
              label: 'Notifications',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.add),
              label: 'Flex',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.leaderboard),
              label: 'Leaderboard',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle),
              label: 'Account',
            ),
          ],
          currentIndex: _page,
          selectedItemColor: MyColors.primaryGreen,
          onTap: navigationTapped,
        ),
        body: new PageView(children: [
          Feed(),
          Notifications(),
          LinesPage(),
          LeaderBoard(),
          Profile()
        ], controller: _pageController, onPageChanged: onPageChanged));
  }

  void navigationTapped(int page) {
    // Animating to the page.
    // You can use whatever duration and curve you like
    _pageController.animateToPage(page,
        duration: const Duration(milliseconds: 300), curve: Curves.ease);
  }

  @override
  void initState() {
    super.initState();
    _pageController = new PageController();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }
}

class NavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return BottomNavigationBar(
      iconSize: 24,
      type: BottomNavigationBarType.fixed,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.notifications),
          label: 'Notifications',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.add),
          label: 'Flex',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.leaderboard),
          label: 'Leaderboard',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person_add_outlined),
          label: 'Account',
        ),
      ],
      currentIndex: 0,
      selectedItemColor: MyColors.primaryGreen,
      onTap: null,
    );
  }
}

class Test {
  String apicall = "";

  _getRequest() async {
    String url =
        "https://betsapi2.p.rapidapi.com/v1/bet365/result?event_id=1673888670";

    final response = await get(
      url,
      headers: {
        "x-rapidapi-key": "5677e70ba2mshfbef6c9f5f96818p19744fjsn25c4b7008942",
        "x-rapidapi-host": "betsapi2.p.rapidapi.com",
        "Host": "betsapi2.p.rapidapi.com",
      },
    );

    print(response.body);
    print(1);
    this.apicall = response.body[0];
    return response;
  }
}
