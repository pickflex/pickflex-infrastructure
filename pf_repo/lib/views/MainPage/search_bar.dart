  import 'package:flutter/material.dart';
import 'package:pf_repo/utils/colors.dart';
import 'package:pf_repo/views/components/text_field_container.dart';

class RoundedSearchInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChanged;
  const RoundedSearchInputField({
    Key key,
    this.hintText,
    this.icon = Icons.person,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: SearchFieldHeaderContainer(
        child: TextField(
          onChanged: onChanged,
          cursorColor: MyColors.primaryGreen,
          decoration: InputDecoration(
            icon: Icon(
              icon,
              color: MyColors.primaryGreen,
            ),
            hintText: hintText,
            border: InputBorder.none,
          ),
        ),
      ),
    );
  }
}

class SearchFieldHeaderContainer extends StatelessWidget {
  final Widget child;
  const SearchFieldHeaderContainer({
    Key key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.4,
      decoration: BoxDecoration(
        color: MyColors.primaryLightGreen,
        borderRadius: BorderRadius.circular(29),
      ),
      child: child,
    );
  }
}
