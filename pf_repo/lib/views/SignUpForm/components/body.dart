import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pf_repo/main.dart';
import 'package:pf_repo/utils/strings.dart';
import 'package:pf_repo/views/MainPage/main_body.dart';
import 'package:pf_repo/views/components/already_have_an_account_check.dart';
import 'package:pf_repo/views/components/rounded_button.dart';
import 'package:pf_repo/views/components/rounded_input_field.dart';
import 'package:pf_repo/views/components/rounded_password_field.dart';
import 'package:pf_repo/views/Login/login_screen.dart';
import 'package:pf_repo/views/components/text_field_container.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'background.dart';

class Body extends StatelessWidget {
  String name;
  String bio;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "SIGNUP",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: size.height * 0.03),
            RoundedInputField(
              hintText: "Name",
              onChanged: (value) {
                this.name = value;
              },
            ),
            RoundedInputField(
              hintText: "Bio",
              onChanged: (value) {
                this.bio = value;
              },
            ),
            RoundedButton(
              text: "DONE",
              press: () {
                addUserInfo(context);
              },
            ),
          ],
        ),
      ),
    );
  }
  // ignore: missing_return
  Future<Widget> addUserInfo(context) async {
    final databaseReference = Firestore.instance;
/*    await databaseReference.collection("users")
        .document("BasicInfo")
        .setData({
      'Email': '${MyStrings.email}',
      'Name': '${this.name}',
      'Bio': '${this.bio}'
    });*/
    try {
      await databaseReference.collection("users")
          .document(MyStrings.email)
          .setData({
        'Email': '${MyStrings.email}',
        'Name': '${this.name}',
        'Bio': '${this.bio}'
      });
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) {
            return MainBody();
          },
        ),
      );
    }
    catch(formError){
      print("SignUpForm error: $formError");
    }
  }
}
