import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pf_repo/utils/colors.dart';

class NotificationCard extends StatelessWidget {
  NotificationCard(
      {this.type, this.username, this.description, this.flex, this.flexid});
  final String type;
  final String username;
  final String description;
  final String flex;
  final String flexid;

  @override
  Widget build(BuildContext context) {
    Widget spreads = Expanded(
      child: Column(
        // align the text to the left instead of centered
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(this.username + " " + this.type + " " + this.flex,
              style: GoogleFonts.adventPro(
                  fontWeight: FontWeight.bold, fontSize: 32))
        ],
      ),
    );

    return Column(children: [
      Container(
          height: 100,
          child: Card(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    IconButton(
                        icon: Icon(
                          Icons.verified_user_outlined,
                          color: Colors.black,
                          size: 56,
                        ),
                        onPressed: null),
                    spreads
                  ],
                ),
              )))
    ]);
  }
}

class CustomButton extends StatelessWidget {
  const CustomButton({this.onPressed, this.text});
  final GestureTapCallback onPressed;
  final String text;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      fillColor: Colors.green,
      splashColor: Colors.greenAccent,
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(
              width: 10.0,
            ),
            Text(
              this.text,
              maxLines: 1,
              style: TextStyle(color: Colors.white),
            ),
            SizedBox(
              width: 10.0,
            ),
          ],
        ),
      ),
      onPressed: onPressed,
      shape: const StadiumBorder(),
    );
  }
}
