import 'dart:convert';
import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pf_repo/utils/colors.dart';

import 'notificationcard.dart';

class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  dynamic notifications;

  void getNotifications() async {
    String data = await DefaultAssetBundle.of(context)
        .loadString("testnotifications.json");
    final jsonResult = json.decode(data);
    notifications = jsonResult;
    print(jsonResult.toString());
    this.setState(() {
      notifications = jsonResult;
    });
  }

  @override
  void initState() {
    this.getNotifications();
  }

  @override
  Widget build(BuildContext context) {
    return new ListView.separated(
      itemCount: notifications == null ? 0 : notifications.length,
      itemBuilder: (BuildContext context, int index) {
        return new NotificationCard(
            type: notifications[index.toString()]["type"],
            username: notifications[index.toString()]["username"],
            description: notifications[index.toString()]["description"],
            flex: notifications[index.toString()]["flex"],
            flexid: notifications[index.toString()]["flexid"]);
      },
      separatorBuilder: (context, index) {
        return Divider();
      },
    );
  }
}
