import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pf_repo/main.dart';
import 'package:pf_repo/utils/strings.dart';
import 'package:pf_repo/views/SignUpForm/signup_screen.dart';
import 'package:pf_repo/views/components/already_have_an_account_check.dart';
import 'package:pf_repo/views/components/rounded_button.dart';
import 'package:pf_repo/views/components/rounded_input_field.dart';
import 'package:pf_repo/views/components/rounded_password_field.dart';
import 'package:pf_repo/views/Login/login_screen.dart';

import 'background.dart';

class Body extends StatelessWidget {
  String password;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "SIGNUP",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: size.height * 0.03),
            RoundedInputField(
              hintText: "Your Email",
              onChanged: (value) {
                MyStrings.email = value;
              },
            ),
            RoundedPasswordField(
              onChanged: (value) {
                this.password = value;
              },
            ),
            RoundedButton(
              text: "SIGNUP",
              press: () {
                registerTheUser(context);
              },
            ),
            SizedBox(height: size.height * 0.03),
            AlreadyHaveAnAccountCheck(
              login: false,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return LoginScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  // ignore: missing_return
  Future<Widget> registerTheUser(context) async {
    print("email ${MyStrings.email}");
    print("password ${this.password}");
    User user;
    UserCredential result;
    try {
      FirebaseAuth firebaseAuth = FirebaseAuth.instance;
      result = await firebaseAuth.createUserWithEmailAndPassword(
          email: MyStrings.email,
          password: this.password
      );
      user = result.user;
      print("Got a new user");
    } catch(signUpError) {
      print("Got an error: $signUpError");
      //if the error is user already existing
      //go to login screen
    }
    if(user != null){
      print("Going to the next screen");
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) {
            return SignUpFormScreen();
          },
        ),
      );
    }
  }
}
