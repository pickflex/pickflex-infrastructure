import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pf_repo/utils/colors.dart';

class LandingHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return AppBar(
        title: Text(
          "SportFlex",
          style:
              GoogleFonts.yatraOne(color: MyColors.primaryGreen, fontSize: 32),
        ),
        backgroundColor: MyColors.primaryGrey,
        actions: [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              "Log In",
              style: GoogleFonts.shadowsIntoLight(
                  color: Colors.white, fontSize: 24),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              "Sign Up",
              style: GoogleFonts.shadowsIntoLight(
                  color: Colors.white, fontSize: 24),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(Icons.more_vert),
          )
        ]);
  }
}
