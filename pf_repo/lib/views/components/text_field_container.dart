import 'package:flutter/material.dart';
import 'package:pf_repo/utils/colors.dart';

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    Key key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.8,
      decoration: BoxDecoration(
        color: MyColors.primaryLightGreen,
        borderRadius: BorderRadius.circular(29),
      ),
      child: child,
    );
  }
}

class TextFieldHeaderContainer extends StatelessWidget {
  final Widget child;
  const TextFieldHeaderContainer({
    Key key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.2,
      decoration: BoxDecoration(
        color: MyColors.primaryLightGreen,
        borderRadius: BorderRadius.circular(29),
      ),
      child: child,
    );
  }
}

class TextFieldHeaderButtonContainer extends StatelessWidget {
  final Widget child;
  const TextFieldHeaderButtonContainer({
    Key key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.2,
      decoration: BoxDecoration(
        color: MyColors.primaryGreen,
        borderRadius: BorderRadius.circular(29),
      ),
      child: child,
    );
  }
}
