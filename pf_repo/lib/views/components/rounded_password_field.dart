import 'package:flutter/material.dart';
import 'package:pf_repo/main.dart';
import 'package:pf_repo/utils/colors.dart';
import 'package:pf_repo/views/Signup/signup_screen.dart';
import 'package:pf_repo/views/components/text_field_container.dart';

class RoundedPasswordField extends StatelessWidget {
  final ValueChanged<String> onChanged;
  const RoundedPasswordField({
    Key key,
    this.onChanged,
  }) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        obscureText: true,
        onChanged: onChanged,
        cursorColor: MyColors.primaryGreen,
        decoration: InputDecoration(
          hintText: "Password",
          icon: Icon(
            Icons.lock,
            color: MyColors.primaryGreen,
          ),
          suffixIcon: Icon(
            Icons.visibility,
            color: MyColors.primaryGreen,
          ),
          border: InputBorder.none,
        ),
      ),
    );
  }
}
