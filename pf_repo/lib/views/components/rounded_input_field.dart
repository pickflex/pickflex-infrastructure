import 'package:flutter/material.dart';
import 'package:pf_repo/utils/colors.dart';
import 'package:pf_repo/views/components/text_field_container.dart';

class RoundedInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChanged;
  const RoundedInputField({
    Key key,
    this.hintText,
    this.icon = Icons.person,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        onChanged: onChanged,
        cursorColor: MyColors.primaryGreen,
        decoration: InputDecoration(
          icon: Icon(
            icon,
            color: MyColors.primaryGreen,
          ),
          hintText: hintText,
          border: InputBorder.none,
        ),
      ),
    );
  }
}