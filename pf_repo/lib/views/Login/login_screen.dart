import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pf_repo/utils/colors.dart';

import 'components/body.dart';
import 'package:pf_repo/views/HomePage/components/landing_header.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[LandingHeaderNoField(), Body()],
        ),
      ),
    );
  }
}
