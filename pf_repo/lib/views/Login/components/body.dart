import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pf_repo/views/MainPage/main_body.dart';
import 'package:pf_repo/views/components/already_have_an_account_check.dart';
import 'package:pf_repo/views/components/rounded_button.dart';
import 'package:pf_repo/views/components/rounded_input_field.dart';
import 'package:pf_repo/views/components/rounded_password_field.dart';
import 'package:pf_repo/views/HomePage/components/background.dart';
import 'package:pf_repo/views/Signup/signup_screen.dart';

class Body extends StatelessWidget {

  String email;
  String password;

  // const Body({
  //   Key key,
  // }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "LOGIN",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: size.height * 0.03),
            SizedBox(height: size.height * 0.03),
            RoundedInputField(
              hintText: "Your Email",
              onChanged: (value) {
                this.email = value;
              },
            ),
            RoundedPasswordField(
              onChanged: (value) {
                this.password = value;
              },
            ),
            RoundedButton(
              text: "LOGIN",
              press: () {
                loginTheUser(context);
              },
            ),
            SizedBox(height: size.height * 0.03),
            AlreadyHaveAnAccountCheck(
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return SignUpScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  // ignore: missing_return
  Future<Widget> loginTheUser(context) async {
    print("email ${this.email}");
    print("password ${this.password}");
    User user;
    UserCredential result;
    try {
      FirebaseAuth firebaseAuth = FirebaseAuth.instance;
      result = await firebaseAuth.signInWithEmailAndPassword(
          email: this.email,
          password: this.password
      );
      user = result.user;
      print("Successful Login");
    } catch (error) {
      print("Login falied code: ${error.code}");
    }
    if (user != null) {
      print("Going to the next screen");
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) {
            return MainBody();
          },
        ),
      );
    }
  }
}