import 'dart:convert';
import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pf_repo/utils/colors.dart';

class LeaderBoard extends StatefulWidget {
  @override
  _LeaderBoardState createState() => _LeaderBoardState();
}

class _LeaderBoardState extends State<LeaderBoard> {
  dynamic users;

  void getLines() async {
    String data =
        await DefaultAssetBundle.of(context).loadString("testusers.json");
    final jsonResult = json.decode(data);
    users = jsonResult;
    print(jsonResult.toString());
    this.setState(() {
      users = jsonResult;
    });
  }

  @override
  void initState() {
    this.getLines();
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Column(children: <Widget>[
        Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          Center(
            child: Text('Friends/Everyone'),
          ),
          Center(
            child: Switch(
              value: true,
              onChanged: null,
              activeTrackColor: Colors.yellow,
              activeColor: MyColors.primaryLightGreen,
            ),
          )
        ]),
        Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          Center(
            child: Text('All-time/Last24'),
          ),
          Center(
            child: Switch(
              value: true,
              onChanged: null,
              activeTrackColor: Colors.yellow,
              activeColor: MyColors.primaryLightGreen,
            ),
          )
        ]),
      ]),
      SizedBox(height: 15),
      Text("LEADERBOARD",
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25)),
      Padding(
        padding: const EdgeInsets.only(top: 30),
      ),
      SingleChildScrollView(
          child: DataTable(
        sortColumnIndex: 0,
        dataRowHeight: 53.0,
        headingRowHeight: 70.0,
        headingRowColor: MaterialStateProperty.all(MyColors.primaryLightGreen),
        columns: [
          DataColumn(
              label: Text(
            "Rank",
            textAlign: TextAlign.start,
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25),
          )),
          DataColumn(
              label: Text(
            "Username",
            textAlign: TextAlign.start,
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25),
          )),
          DataColumn(
              label: Text(
            "Record",
            textAlign: TextAlign.start,
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25),
          )),
          DataColumn(
              label: Text(
            "Last 24 Hours",
            textAlign: TextAlign.start,
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25),
          )),
          DataColumn(
              label: Text(
            "Last 7 Days",
            textAlign: TextAlign.start,
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25),
          )),
        ],
        rows: [
          DataRow(cells: [
            DataCell(Text(users["0"]["rank"])),
            DataCell(Text(users["0"]["username"])),
            DataCell(Text(users["0"]["record"])),
            DataCell(Text(users["0"]["last24"])),
            DataCell(Text(users["0"]["last7"])),
          ]),
          DataRow(cells: [
            DataCell(Text(users["1"]["rank"])),
            DataCell(Text(users["1"]["username"])),
            DataCell(Text(users["1"]["record"])),
            DataCell(Text(users["1"]["last24"])),
            DataCell(Text(users["1"]["last7"])),
          ]),
          DataRow(cells: [
            DataCell(Text(users["2"]["rank"])),
            DataCell(Text(users["2"]["username"])),
            DataCell(Text(users["2"]["record"])),
            DataCell(Text(users["2"]["last24"])),
            DataCell(Text(users["2"]["last7"])),
          ]),
          DataRow(cells: [
            DataCell(Text(users["3"]["rank"])),
            DataCell(Text(users["3"]["username"])),
            DataCell(Text(users["3"]["record"])),
            DataCell(Text(users["3"]["last24"])),
            DataCell(Text(users["3"]["last7"])),
          ]),
          DataRow(cells: [
            DataCell(Text(users["4"]["rank"])),
            DataCell(Text(users["4"]["username"])),
            DataCell(Text(users["4"]["record"])),
            DataCell(Text(users["4"]["last24"])),
            DataCell(Text(users["4"]["last7"])),
          ]),
          DataRow(cells: [
            DataCell(Text(users["5"]["rank"])),
            DataCell(Text(users["5"]["username"])),
            DataCell(Text(users["5"]["record"])),
            DataCell(Text(users["5"]["last24"])),
            DataCell(Text(users["5"]["last7"])),
          ]),
          DataRow(cells: [
            DataCell(Text(users["6"]["rank"])),
            DataCell(Text(users["6"]["username"])),
            DataCell(Text(users["6"]["record"])),
            DataCell(Text(users["6"]["last24"])),
            DataCell(Text(users["6"]["last7"])),
          ]),
          DataRow(cells: [
            DataCell(Text(users["7"]["rank"])),
            DataCell(Text(users["7"]["username"])),
            DataCell(Text(users["7"]["record"])),
            DataCell(Text(users["7"]["last24"])),
            DataCell(Text(users["7"]["last7"])),
          ])
        ],
      ))
    ]);
  }
}
