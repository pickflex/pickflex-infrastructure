import 'dart:js';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pf_repo/utils/colors.dart';

import 'package:pf_repo/views/HomePage/components/landing_header.dart';
import 'package:pf_repo/views/HomePage/components/split_view.dart';

import 'components/body.dart';
import 'components/bottom_navigation.dart';
import 'components/main_header.dart';

class Feed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          new Card(
            child: Container(
              alignment: Alignment.topCenter,
              margin: null,
              width: 800,
              height: 125,
              child: Column(children: <Widget>[
                Row(crossAxisAlignment: CrossAxisAlignment.center, children: <
                    Widget>[
                  Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.fromLTRB(3.0, 3.0, 3.0, 0),
                      child: IconButton(
                        icon: Icon(Icons.face, color: MyColors.primaryGreen),
                        color: MyColors.primaryGreen,
                        iconSize: 40,
                        padding: EdgeInsets.fromLTRB(3.0, 3.0, 3.0, 0),
                      )),
                  Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.symmetric(vertical: 0),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: Container(
                          width: 700,
                          height: 20,
                          alignment: Alignment.topCenter,
                          padding: EdgeInsets.fromLTRB(2.0, 0, 2.0, 0),
                          child: Row(children: <Widget>[
                            Container(
                                width: 200,
                                padding: EdgeInsets.fromLTRB(15.0, 0, 0, 0),
                                alignment: Alignment.topCenter,
                                child: Text("TheFlexer",
                                    style: GoogleFonts.aclonica(
                                        fontSize: 24,
                                        color: MyColors.primaryGrey))),
                            Container(
                                width: 450,
                                padding: EdgeInsets.fromLTRB(15.0, 1.0, 2.0, 0),
                                alignment: Alignment.topRight,
                                child: new Text(
                                    DateTime.now().month.toString() +
                                        "-" +
                                        DateTime.now().day.toString() +
                                        " " +
                                        DateTime.now().hour.toString() +
                                        ":" +
                                        DateTime.now().minute.toString(),
                                    style: GoogleFonts.beVietnam(fontSize: 12)))
                          ])))
                ]),
                Row(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(5.0, 1.0, 2.0, 0),
                      child: IconButton(
                        icon:
                            Icon(Icons.sports_basketball, color: Colors.brown),
                        color: Colors.brown,
                        iconSize: 56,
                      ),
                    ),
                    Container(
                        alignment: Alignment.bottomRight,
                        padding: EdgeInsets.fromLTRB(10.0, 1.0, 2.0, 0),
                        width: 100,
                        height: 64,
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              TextButton(
                                  onPressed: null,
                                  child: Text("Comment",
                                      style:
                                          GoogleFonts.comicNeue(fontSize: 16))),
                            ])),
                    Container(
                        alignment: Alignment.topRight,
                        padding: EdgeInsets.fromLTRB(15.0, 1.0, 2.0, 0),
                        width: 375,
                        height: 64,
                        child: Text("Wizards (-7) vs Lakers",
                            style: GoogleFonts.tajawal(
                                fontSize: 32, fontWeight: FontWeight.bold))),
                    Container(
                        alignment: Alignment.bottomRight,
                        padding: EdgeInsets.fromLTRB(5.0, 1.0, 2.0, 0),
                        width: 175,
                        height: 64,
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              IconButton(
                                icon: Icon(Icons.thumb_up,
                                    color: MyColors.primaryGrey),
                                color: MyColors.primaryGrey,
                                iconSize: 16,
                              ),
                              IconButton(
                                icon: Icon(Icons.thumb_down,
                                    color: MyColors.primaryGrey),
                                color: MyColors.primaryGrey,
                                iconSize: 16,
                              ),
                            ])),
                  ],
                )
              ]),
              decoration: BoxDecoration(
                border: Border.all(color: MyColors.primaryGreen, width: 2.0),
              ),
            ),
            shadowColor: MyColors.primaryGreen,
            color: MyColors.primaryLightGreen,
          )
        ]),
        Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          new Card(
            child: Container(
              alignment: Alignment.topCenter,
              margin: null,
              width: 800,
              height: 125,
              child: Column(children: <Widget>[
                Row(crossAxisAlignment: CrossAxisAlignment.center, children: <
                    Widget>[
                  Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.fromLTRB(3.0, 3.0, 3.0, 0),
                      child: IconButton(
                        icon: Icon(Icons.face, color: MyColors.primaryGreen),
                        color: MyColors.primaryGreen,
                        iconSize: 40,
                        padding: EdgeInsets.fromLTRB(3.0, 3.0, 3.0, 0),
                      )),
                  Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.symmetric(vertical: 0),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: Container(
                          width: 700,
                          height: 20,
                          alignment: Alignment.topCenter,
                          padding: EdgeInsets.fromLTRB(2.0, 0, 2.0, 0),
                          child: Row(children: <Widget>[
                            Container(
                                width: 200,
                                padding: EdgeInsets.fromLTRB(15.0, 0, 0, 0),
                                alignment: Alignment.topCenter,
                                child: Text("Schmeatty",
                                    style: GoogleFonts.aclonica(
                                        fontSize: 24,
                                        color: MyColors.primaryGrey))),
                            Container(
                                width: 450,
                                padding: EdgeInsets.fromLTRB(15.0, 1.0, 2.0, 0),
                                alignment: Alignment.topRight,
                                child: new Text(
                                    DateTime.now().month.toString() +
                                        "-" +
                                        DateTime.now().day.toString() +
                                        " " +
                                        DateTime.now().hour.toString() +
                                        ":" +
                                        DateTime.now().minute.toString(),
                                    style: GoogleFonts.beVietnam(fontSize: 12)))
                          ])))
                ]),
                Row(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(5.0, 1.0, 2.0, 0),
                      child: IconButton(
                        icon:
                            Icon(Icons.sports_basketball, color: Colors.brown),
                        color: Colors.brown,
                        iconSize: 56,
                      ),
                    ),
                    Container(
                        alignment: Alignment.bottomRight,
                        padding: EdgeInsets.fromLTRB(10.0, 1.0, 2.0, 0),
                        width: 100,
                        height: 64,
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              TextButton(
                                  onPressed: null,
                                  child: Text("Comment",
                                      style:
                                          GoogleFonts.comicNeue(fontSize: 16))),
                            ])),
                    Container(
                        alignment: Alignment.topRight,
                        padding: EdgeInsets.fromLTRB(15.0, 1.0, 2.0, 0),
                        width: 375,
                        height: 64,
                        child: Text("Wizards vs Lakers (+7)",
                            style: GoogleFonts.tajawal(
                                fontSize: 32, fontWeight: FontWeight.bold))),
                    Container(
                        alignment: Alignment.bottomRight,
                        padding: EdgeInsets.fromLTRB(5.0, 1.0, 2.0, 0),
                        width: 175,
                        height: 64,
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              IconButton(
                                icon: Icon(Icons.thumb_up,
                                    color: MyColors.primaryGrey),
                                color: MyColors.primaryGrey,
                                iconSize: 16,
                              ),
                              IconButton(
                                icon: Icon(Icons.thumb_down,
                                    color: MyColors.primaryGrey),
                                color: MyColors.primaryGrey,
                                iconSize: 16,
                              ),
                            ])),
                  ],
                )
              ]),
              decoration: BoxDecoration(
                border: Border.all(color: MyColors.primaryGrey, width: 2.0),
              ),
            ),
            shadowColor: MyColors.primaryGreen,
            color: MyColors.primaryLightGreen,
          )
        ])
      ],
    );
  }
}
