import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pf_repo/utils/colors.dart';
import 'package:pf_repo/views/components/rounded_button.dart';

class Flex extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(children: <Widget>[FlexTile(), FlexTile(), FlexTile()]);
  }
}

class FlexTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: new EdgeInsets.all(20.0),
      color: MyColors.primaryLightGreen,
      child: new Center(
        child: new Row(
          children: <Widget>[
            new Card(
                child: Container(
                    alignment: Alignment.topCenter,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("Flex", style: GoogleFonts.amiko(fontSize: 32))
                        ])),
                color: MyColors.primaryGrey),
            // new Text(
            //   judul,
            //   style: new TextStyle(fontSize: 20.0),
          ],
        ),
      ),
    );
  }
}
