import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pf_repo/utils/colors.dart';
import 'package:pf_repo/views/Login/login_screen.dart';
import 'package:pf_repo/schema/Flex.dart';

import 'package:pf_repo/views/Feed/components/flex_feed.dart';
import 'background.dart';
import 'flex_feed.dart';
import 'package:http/http.dart';

import 'main_header.dart';

class OuterBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // This size provide us total height and width of our screen
    return Background(child: MainBody());
  }
}

class MainBody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new FlexListState();
  }
}

class FlexListState extends State<MainBody> {
  PageController _pageController;

  int _page = 0;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: MainHeader().build(context),
        backgroundColor: Colors.white,
        bottomNavigationBar: new BottomNavigationBar(
          iconSize: 24,
          type: BottomNavigationBarType.fixed,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.notifications),
              label: 'Notifications',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.add),
              label: 'Flex',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.leaderboard),
              label: 'Leaderboard',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person_add_outlined),
              label: 'Account',
            ),
          ],
          currentIndex: _page,
          selectedItemColor: MyColors.primaryGreen,
          onTap: navigationTapped,
        ),
        body: new PageView(children: [
          new Container(color: Colors.red),
          new Container(color: Colors.blue),
          new Container(color: Colors.grey),
          new Container(color: Colors.white),
          new Container(color: MyColors.primaryLightGreen)
        ], controller: _pageController, onPageChanged: onPageChanged));
  }

  Future<String> _getRequest() async {
    String url = "https://api.pinnacle.com/v2/sport";

    final response = await get(
      url,
      headers: {
        "x-rapidapi-key": "",
        "x-rapidapi-host": "pinnacle-odds.p.rapidapi.com",
        "useQueryString": "true",
      },
    );
    print(1);
    print(response.body);
  }

  void navigationTapped(int page) {
    // Animating to the page.
    // You can use whatever duration and curve you like
    _pageController.animateToPage(page,
        duration: const Duration(milliseconds: 300), curve: Curves.ease);
  }

  @override
  void initState() {
    super.initState();
    _pageController = new PageController();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }
}

class NavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return BottomNavigationBar(
      iconSize: 24,
      type: BottomNavigationBarType.fixed,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.notifications),
          label: 'Notifications',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.add),
          label: 'Flex',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.leaderboard),
          label: 'Leaderboard',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person_add_outlined),
          label: 'Account',
        ),
      ],
      currentIndex: 0,
      selectedItemColor: MyColors.primaryGreen,
      onTap: null,
    );
  }
}
