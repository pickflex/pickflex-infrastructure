import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pf_repo/utils/colors.dart';

import 'search_bar.dart';

class MainHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return AppBar(
        title: Text(
          "SportFlex",
          style:
              GoogleFonts.yatraOne(color: MyColors.primaryGreen, fontSize: 32),
        ),
        backgroundColor: MyColors.primaryGrey,
        leading: Image(
            image: AssetImage(
          "../lib/assets/images/logo.png",
        )),
        actions: [
          Padding(
            padding: EdgeInsets.all(2.0),
            child: RoundedSearchInputField(
              hintText: "Search",
              onChanged: (value) {},
            ),
          ),
          Padding(
            padding: EdgeInsets.all(2.0),
            child: IconButton(
              icon: Icon(Icons.search, color: MyColors.primaryGreen),
              color: MyColors.primaryGreen,
              iconSize: 32,
            ),
          ),
          SizedBox(height: size.height * 0.05),
          Padding(
              padding: EdgeInsets.all(2.0),
              child: Padding(
                padding: EdgeInsets.all(2.0),
                child: null,
              )),
          Padding(
            padding: EdgeInsets.all(2.0),
            child: IconButton(
              icon: Icon(Icons.settings, color: MyColors.primaryGreen),
              color: MyColors.primaryGreen,
              iconSize: 32,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(2.0),
            child: IconButton(
              icon: Icon(Icons.person, color: MyColors.primaryGreen),
              color: MyColors.primaryGreen,
              iconSize: 32,
            ),
          )
        ]);
  }
}
