import 'dart:convert';
import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'linecard.dart';

class LinesPage extends StatefulWidget {
  @override
  _FlexInformationState createState() => new _FlexInformationState();
}

class _FlexInformationState extends State<LinesPage> {
  dynamic lines;

  CollectionReference nbalines = FirebaseFirestore.instance
      .collection('dailylines')
      .doc('JmUqZusU8dMBOKQgh3OO')
      .collection('mayfifth')
      .doc('n8WwA1aYULtch4g1zq0z')
      .collection('nba');

  Stream nbastream = FirebaseFirestore.instance
      .collection('dailylines')
      .doc('JmUqZusU8dMBOKQgh3OO')
      .collection('mayfifth')
      .doc('n8WwA1aYULtch4g1zq0z')
      .collection('nba')
      .snapshots();

  CollectionReference nhllines = FirebaseFirestore.instance
      .collection('dailylines')
      .doc('JmUqZusU8dMBOKQgh3OO')
      .collection('mayfourth')
      .doc('dfsTZQmAJmprm9oaTGIk')
      .collection('nhl');

  void getLines() async {
    String data =
        await DefaultAssetBundle.of(context).loadString("dailylines.json");
    final jsonResult = json.decode(data);
    lines = jsonResult;
    print(jsonResult.toString());
    this.setState(() {
      lines = jsonResult;
    });
  }

  @override
  void initState() {
    this.getLines();
  }

  @override
  Widget build(BuildContext context) {
    //return Text(this.lines["1"].toString());

    return new ListView.separated(
      itemCount: lines == null ? 0 : lines.length,
      itemBuilder: (BuildContext context, int index) {
        return new LineCard(
            team1: lines[index.toString()]["team1"],
            team1logo: lines[index.toString()]["team1icon"],
            team2: lines[index.toString()]["team2"],
            team2logo: lines[index.toString()]["team2icon"],
            date: lines[index.toString()]["time"],
            spread1: lines[index.toString()]["spread"][0],
            spread2: lines[index.toString()]["spread"][1],
            overunder: lines[index.toString()]["o/u"]);
      },
      separatorBuilder: (context, index) {
        return Divider();
      },
    );
  }
}

//return new ListTile(
//          leading: IconButton(
//              icon: Icon(Icons.sports_basketball, color: Colors.brown),
//              onPressed: null),
//          title: new Text(
//            lines[index.toString()]["team1"] +
//                " vs. " +
//                lines[index.toString()]["team2"],
//            textAlign: TextAlign.center,
//          ),
//          subtitle: new Text("Spread: " +
//              lines[index.toString()]["spread"] +
//              " Over/Under: " +
//              lines[index.toString()]["o/u"]),
//       );
