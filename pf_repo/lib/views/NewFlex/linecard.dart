import 'dart:convert';
import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pf_repo/utils/colors.dart';

import 'custombutton.dart';

class LineCard extends StatelessWidget {
  LineCard(
      {this.team1,
      this.team1logo,
      this.team2,
      this.team2logo,
      this.date,
      this.spread1,
      this.spread2,
      this.overunder});
  final String team1;
  final String team1logo;
  final String team2;
  final String team2logo;
  final String date;
  final String spread1;
  final String spread2;
  final String overunder;

  @override
  Widget build(BuildContext context) {
    Widget _buildPopupDialogOver(BuildContext context) {
      return new AlertDialog(
        title: Text("Over " + this.overunder),
        content: new Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextField(),
          ],
        ),
        actions: <Widget>[
          CustomButton(onPressed: () {}, text: "Flex"),
          new FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            textColor: Theme.of(context).primaryColor,
            child: const Text('Close'),
          ),
        ],
      );
    }

    Widget _buildPopupDialogUnder(BuildContext context) {
      return new AlertDialog(
        title: Text("Under " + this.overunder),
        content: new Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextField(),
          ],
        ),
        actions: <Widget>[
          CustomButton(onPressed: () {}, text: "Flex"),
          new FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            textColor: Theme.of(context).primaryColor,
            child: const Text('Close'),
          ),
        ],
      );
    }

    Widget spreads = Expanded(
      child: Column(
        // align the text to the left instead of centered
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text("Spreads",
              style: GoogleFonts.adventPro(
                  fontWeight: FontWeight.bold, fontSize: 32)),
          CustomButton(onPressed: () {}, text: this.spread1),
          CustomButton(onPressed: () {}, text: this.spread2),
        ],
      ),
    );

    Widget totals = Expanded(
      child: Column(
        // align the text to the left instead of centered
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text("Over/Under",
              style: GoogleFonts.adventPro(
                  fontWeight: FontWeight.bold, fontSize: 32)),
          CustomButton(
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) =>
                      _buildPopupDialogOver(context),
                );
              },
              text: "Over " + this.overunder + " "),
          CustomButton(
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) =>
                      _buildPopupDialogUnder(context),
                );
              },
              text: "Under " + this.overunder)
        ],
      ),
    );

    Widget logo1 = Expanded(
      child: Column(
        // align the text to the left instead of centered
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image(
              width: 100,
              height: 100,
              image: AssetImage(
                "../lib/assets/images/" + this.team1logo + ".png",
              ))
        ],
      ),
    );

    Widget nametime = Expanded(
      child: Column(
        // align the text to the left instead of centered
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(children: <Widget>[
            Column(children: <Widget>[
              Image(
                  width: 75,
                  height: 75,
                  image: AssetImage(
                    "../lib/assets/images/" + this.team1logo + ".png",
                  )),
              SizedBox(
                height: 10.0,
              ),
              Image(
                  width: 75,
                  height: 75,
                  image: AssetImage(
                    "../lib/assets/images/" + this.team2logo + ".png",
                  ))
            ]),
            SizedBox(
              width: 10.0,
            ),
            Column(children: <Widget>[
              Text(this.team1,
                  style: GoogleFonts.adventPro(
                      fontWeight: FontWeight.bold, fontSize: 32)),
              Text(" vs. ",
                  style: GoogleFonts.adventPro(
                      fontWeight: FontWeight.bold, fontSize: 32)),
              Text(this.team2,
                  style: GoogleFonts.adventPro(
                      fontWeight: FontWeight.bold, fontSize: 32)),
              Text(this.date,
                  style: GoogleFonts.adventPro(
                      fontWeight: FontWeight.bold, fontSize: 24))
            ]),
          ])
        ],
      ),
    );

    return Column(children: [
      Container(
          height: 200,
          child: Card(
              color: MyColors.primaryLightGreen,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    IconButton(
                        icon: Icon(
                          Icons.sports_basketball,
                          color: Colors.brown,
                          size: 56,
                        ),
                        onPressed: null),
                    spreads,
                    nametime,
                    totals
                  ],
                ),
              )))
    ]);
  }
}
