class Lines {
  int _id;
  String _team1;
  String _team2;

  Lines(this._team1, this._team2);

  Lines.fromMap(dynamic obj) {
    this._team1 = obj['team1'];
    this._team2 = obj['team2'];
  }

  String get team1 => _team1;
  String get team2 => _team2;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["team1"] = _team1;
    map["team2"] = _team2;
    return map;
  }
}
