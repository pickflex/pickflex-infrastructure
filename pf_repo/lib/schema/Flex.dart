class FlexSchema {
  int id;
  String name;
  String flex;
  int postCount;

  FlexSchema({this.id, this.name, this.flex, this.postCount});
}
