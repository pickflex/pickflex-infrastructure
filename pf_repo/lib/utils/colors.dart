import 'package:flutter/material.dart';

class MyColors {
  static const Color white1 = Color(0xFFF8FBFF);
  static const Color white2 = Color(0xFFFCFDFD);

  static const Color primaryGreen = Color.fromARGB(255, 93, 205, 17);
  static const Color primaryLightGreen = Color.fromARGB(255, 209, 255, 189);
  static const Color primaryGrey = Color.fromARGB(255, 62, 62, 62);
  static const Color primarylightGrey = Color.fromARGB(255, 224, 224, 224);
}
