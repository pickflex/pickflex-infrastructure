import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class UserModel {
  final String id;
  final String name;
  final String email;
  final String image;
  final String record;
  final int followers;
  final DateTime joined;
  final int flexs;

  const UserModel({
    @required this.id,
    @required this.name,
    @required this.email,
    @required this.image,
    @required this.record,
    @required this.followers,
    @required this.joined,
    @required this.flexs,
  });

  String get postTimeFormatted => DateFormat.yMMMMEEEEd().format(joined);
}
