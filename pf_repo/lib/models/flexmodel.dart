import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'usermodel.dart';

class PostModel {
  final String id, flex, description;
  final DateTime postTime;
  final int reacts, views;
  final UserModel author;

  const PostModel({
    @required this.id,
    @required this.flex,
    @required this.description,
    @required this.author,
    @required this.postTime,
    @required this.reacts,
    @required this.views,
  });

  String get postTimeFormatted => DateFormat.yMMMMEEEEd().format(postTime);
}
