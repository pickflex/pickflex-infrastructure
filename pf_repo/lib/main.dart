import 'package:firebase_auth/firebase_auth.dart';
import  'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'views/HomePage/home_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(PickFlex());
}

class PickFlex extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _PickFlexState();
  }
}

class _PickFlexState extends State<PickFlex> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.green,
        scaffoldBackgroundColor: Colors.white,
        brightness: Brightness.light,
      ),
      home: HomePage(),
    );
  }
}
